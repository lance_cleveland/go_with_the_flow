#Go With The Flow

A plugin for Selenium IDE based on SideFlow.

Adds:

 * gotoIf <condition> [label]  - label is optional , if it is skipped it goes to the next label after the GoTo 
 * ifTrue <condition> - when true execute next line, otherwise skip to the next endIf
 * ifFalse <condition> - when false execute next line, otherwise skip to the next endIf
 * endIf - marker for end of if block
 
#Installation

Install Selenium IDE in Firefox.

Add the go_with_the_flow.js file to the Core Extensions under options from with Selenium

Restart Selenium.

#Commands

##ifTrue

Target is a JavaScript expression to be evaluated.

If the condition is true the next command is executed.

If the condition if false the script will skip to the next endIf command or end of the script.

##ifTrue

Target is a JavaScript expression to be evaluated.

If the condition if true the script will skip to the next endIf command or end of the script.

If the condition is false the next command is executed.


##endIf

Mark the end of the if.  

##gotoIf

Target is a JavaScript expression to be evaluated.
If the express is false , execution continues on the next line of the test.

Value is a label name or left empty.    
If the label is NOT specified the next label command found after the gotoIf will be where execution continues.
If the label is specified the 'label' command with the target set to this value will be where execution continues.


##label

A marker that can be jumped to by gotoIf.

Target can be a name that is referenced by a gotoIf command in the value setting.






 