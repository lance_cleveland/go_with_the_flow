/**
 * Go With The Flow
 * go_with_the_flow.js is a script plugin for Selenium IDE based on SideFlow.
 *
 * @version 1.0.0
 * @author Lance Cleveland <lance@lance.bio>
 */

var gotoLabels= {};
var whileLabels = {};

// overload the original Selenium reset function
Selenium.prototype.reset = function() {
    // reset the labels
    this.initialiseLabels();
    // proceed with original reset code
    this.defaultTimeout = Selenium.DEFAULT_TIMEOUT;
    this.browserbot.selectWindow("null");
    this.browserbot.resetPopups();
}


/*
 * ---   Initialize Conditional Elements  --- *
 *  Run through the script collecting line numbers of all conditional elements
 *  There are three a results arrays: goto labels, while pairs and forEach pairs
 *  
 */
Selenium.prototype.initialiseLabels = function()
{
    gotoLabels = {};
    whileLabels = { ends: {}, whiles: {} };
    var command_rows = [];
    var numCommands = testCase.commands.length;
    for (var i = 0; i < numCommands; ++i) {
        var x = testCase.commands[i];
        command_rows.push(x);
    }
    var cycles = [];
    var forEachCmds = [];
    for( var i = 0; i < command_rows.length; i++ ) {
        if (command_rows[i].type == 'command')
        switch( command_rows[i].command.toLowerCase() ) {
            case "label":
                gotoLabels[ command_rows[i].target ] = i;
                break;
            case "while":
            case "endwhile":
                cycles.push( [command_rows[i].command.toLowerCase(), i] )
                break;
            case "foreach":
            case "endforeach":
                forEachCmds.push( [command_rows[i].command.toLowerCase(), i] )
                break;
        }
    }  
    var i = 0;
    while( cycles.length ) {
        if( i >= cycles.length ) {
            throw new Error( "non-matching while/endWhile found" );
        }
        switch( cycles[i][0] ) {
            case "while":
                if( ( i+1 < cycles.length ) && ( "endwhile" == cycles[i+1][0] ) ) {
                    // pair found
                    whileLabels.ends[ cycles[i+1][1] ] = cycles[i][1];
                    whileLabels.whiles[ cycles[i][1] ] = cycles[i+1][1];
                    cycles.splice( i, 2 );
                    i = 0;
                } else ++i;
                break;
            case "endwhile":
                ++i;
                break;
        }
    }
}

Selenium.prototype.continueFromRow = function( row_num )
{
    if(row_num == undefined || row_num == null || row_num < 0) {
        throw new Error( "Invalid row_num specified." );
    }
    testCase.debugContext.debugIndex = row_num;
}

// do nothing. simple label
Selenium.prototype.doLabel = function(){};

Selenium.prototype.doGotoLabel = function( label )
{
    if( undefined == gotoLabels[label] ) {
        throw new Error( "Specified label '" + label + "' is not found." );
    }
    this.continueFromRow( gotoLabels[ label ] );
};

Selenium.prototype.doGoto = Selenium.prototype.doGotoLabel;

Selenium.prototype.doGotoIf = function( condition, label )
{
    if( eval(condition) ) {

        // Label, go to that row
        if ( label !== '' ) {
            this.doGotoLabel(label);

        // no label specified go to the next one
        } else {
            var last_row = testCase.debugContext.debugIndex;
            var numCommands = testCase.commands.length;
            for (var i = last_row; i < numCommands; ++i) {
                if (
                    ( testCase.commands[i].type == 'command' ) &&
                    ( testCase.commands[i].command.toLowerCase() == 'label' )
                ) {
                    this.continueFromRow( i );
                    break;
                }
            }
        }
    }
};


/**
 * Do If
 *
 * Execute the next line based on condiation and true_or_not_flag.
 *
 * @param string condition    - the JavaScript to evaluation for true|false
 * @param boolean true_or_not - if true keep the condition as-is, if false flip the logic.
 *
 */
Selenium.prototype.doIf = function ( condition, true_or_not ) {
    var current_row = testCase.debugContext.debugIndex;
    var statement_true = eval( condition );
    if ( ! true_or_not ) {
        statement_true = ! statement_true;
    }
    if( statement_true ) {
        this.continueFromRow( current_row + 1 );
    } else {
        var numCommands = testCase.commands.length;
        for (var i = current_row; i < numCommands; ++i) {
            if (
                ( testCase.commands[i].type == 'command' ) &&
                ( testCase.commands[i].command.toLowerCase() == 'endif' )
            ) {
                this.continueFromRow( i );
                break;
            }
        }
    }
};

/**
 * IfTrue <condition>
 *
 * @param condition
 */
Selenium.prototype.doIfTrue = function( condition ) {
    this.doIf( condition , true );
};

/**
 * IfFalse <condition>
 *
 * @param condition
 */
Selenium.prototype.doIfFalse = function( condition ) {
    this.doIf( condition , false );
};

/**
 * EndIf
 */
Selenium.prototype.doEndIf = function(){};

Selenium.prototype.doWhile = function( condition )
{
    if( !eval(condition) ) {
        var last_row = testCase.debugContext.debugIndex;
        var end_while_row = whileLabels.whiles[ last_row ];
        if( undefined == end_while_row ) throw new Error( "Corresponding 'endWhile' is not found." );
        this.continueFromRow( end_while_row );
    }
}

Selenium.prototype.doEndWhile = function()
{
    var last_row = testCase.debugContext.debugIndex;
    var while_row = whileLabels.ends[ last_row ] - 1;
    if( undefined == while_row ) throw new Error( "Corresponding 'While' is not found." );
    this.continueFromRow( while_row );
}

Selenium.prototype.doPush= function(value, varName)
{
    if(!storedVars[varName]) {
        storedVars[varName] = new Array();
    } 
    if(typeof storedVars[varName] !== 'object') {
        throw new Error("Cannot push value onto non-array " + varName);
    } else {
        storedVars[varName].push(value);
    }
}

